package com.ivandelgadog.ventasdomiciliog1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {

    ImageView ivLogo;
    TextInputEditText etEmail;
    TextInputEditText etPassword;
    AppCompatButton btnLogin;
    AppCompatButton btnFacebook;
    AppCompatButton btnGoogle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ivLogo = findViewById(R.id.iv_logo);
        etEmail = findViewById(R.id.et_email);
        //etEmail.setText("ivandelgadog@gmail.com");
        etPassword = findViewById(R.id.et_clave);
        //etPassword.setText("miclave");

        btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener((evt)-> onLoginClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener((evt)-> onFacebookClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener((evt)-> onGoogleClick());
    }

    private void onLoginClick(){
        /*etEmail.setText("");
        etPassword.setText("");
        Toast.makeText(this,"Presionó", Toast.LENGTH_SHORT).show(); */
        Intent intent = new Intent(this, PaymentsActivity.class);
        startActivity(intent);
    }

    private void onFacebookClick(){

    }

    private void onGoogleClick(){

    }
}